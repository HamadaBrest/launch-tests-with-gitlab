import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { mount, configure, shallow } from "enzyme";

import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

describe("my app", () => {
  it("test if input exists", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('input').length).toBe(1)
  });

  it("write on input", () => {
    const wrapper = shallow(<App />);
    wrapper.find('input').simulate('change', {target: {value: 'hello world'}});
    expect(wrapper.find('input').props().value).toBe('hello world')
  });
});
