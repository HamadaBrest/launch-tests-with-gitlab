import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
  constructor() {
    super();
    this.state = {
      value: ""
    };
  }

  handleValue = ({ target: { value } }) => {
    this.setState({ value });
  };

  render() {
    return (
      <div className="App">
        <br />
        <input
          type="text"
          value={this.state.value}
          onChange={this.handleValue}
        />
      </div>
    );
  }
}

export default App;
